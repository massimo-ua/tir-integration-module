const endpoints = {
    path_get_campaigns: '/api/campaign/list/',
    path_get_whitelists: '/api/users/author-lists/',
    path_get_jobs: '/api/job/get/',
    path_get_authors: '/api/users/list/',
    path_update_job: '/api/job/status/',
    path_add_order: '/api/order/add/',
    path_get_texttypes: '/api/text-type/list/',
};

const defaults = {
    ...endpoints,
    jobs_query_period: 7,
};

const ClientFactory = Client => ({
    path_get_campaigns,
    path_get_whitelists,
    path_get_texttypes,
    path_get_jobs,
    path_get_authors,
    path_update_job,
    path_add_order,
    jobs_query_period,
    userSecret,
    userName,
    host,
}, options = {}) => Object.freeze(Client({
    ...defaults,
    path_get_campaigns,
    path_get_whitelists,
    path_get_texttypes,
    path_get_jobs,
    path_get_authors,
    path_update_job,
    path_add_order,
    jobs_query_period,
    userSecret,
    userName,
    host,
}, options));

module.exports = ClientFactory;

const JsonHTTPClient = require('./json-http-client');
const { parseAddOrderResponce } = require('./helpers');

const httpClient = jest.fn();
const queryString = jest.fn();
const jobsQueryString = jest.fn();

const config = {
    path_get_campaigns: 'test',
    path_get_whitelists: 'test',
    path_get_texttypes: 'test',
    path_get_jobs: 'test',
    path_get_authors: 'test',
    path_update_job: 'test',
    path_add_order: 'test',
    jobs_query_period: 1,
    userSecret: 'test',
    userName: 'test',
    host: 'test',
};

describe('[JsonHTTPClient]', () => {
    let client;

    beforeEach(() => {
        client = JsonHTTPClient(httpClient, queryString, jobsQueryString);
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should be a function', () => {
        expect(typeof client)
            .toStrictEqual('function');
    });

    it('should create client factory even if params was not passed', () => {
        const defaultClient = JsonHTTPClient();
        expect(typeof defaultClient)
            .toStrictEqual('function');
    });

    it('should return an object with expected properties on call', () => {
        const c = client({});
        expect(Object.keys(c))
            .toStrictEqual([
                'addOrder',
                'approveJob',
                'returnJob',
                'authors',
                'getJobsByOrderId',
                'getCampaigns',
                'getWhiteLists',
                'getOrderTextTypes',
            ]);
    });

    it('should call httpClient with certain params on addOrder', () => {
        queryString.mockImplementation((...args) => args.join(''));
        const payload = { foo: 'bar' };
        const c = client(config, { defaultRequestOptions: {} });
        c.addOrder(payload);
        expect(httpClient)
            .toHaveBeenCalledWith({
                method: 'POST',
                uri: 'testtest',
                qs: 'testtesttest',
                body: payload,
            });
    });

    it('should return errors', () => {
        const payload = {
            errors: [
                {
                    id: 1,
                    message: 'error message',
                },
            ],
        };
        expect(() => {
            parseAddOrderResponce(payload);
        })
            .toThrow();
    });

    it('should return correct object', () => {
        const payload = {
            data: {
                order: 1,
            },
        };
        const result = parseAddOrderResponce(payload);
        expect(result)
            .toStrictEqual({ data: { id_order: 1 } });
    });

    it('should call httpClient with certain params on approveJob', () => {
        queryString.mockImplementation((...args) => args.join(''));
        const payload = {
            jobId: 1,
            manager: 'test',
        };
        const c = client(config, { defaultRequestOptions: {} });
        c.approveJob(payload);
        expect(httpClient)
            .toHaveBeenCalledWith({
                method: 'POST',
                uri: 'testtest',
                qs: 'testtesttest',
                body: {
                    job_id: payload.jobId,
                    manager: payload.manager,
                    transition: 'confirm',
                    message: '',
                },
            });
    });

    it('should call httpClient with certain params on returnJob', () => {
        queryString.mockImplementation((...args) => args.join(''));
        const payload = {
            jobId: 1,
            manager: 'test',
            message: 'test',
        };
        const c = client(config, { defaultRequestOptions: {} });
        c.returnJob(payload);
        expect(httpClient)
            .toHaveBeenCalledWith({
                method: 'POST',
                uri: 'testtest',
                qs: 'testtesttest',
                body: {
                    job_id: payload.jobId,
                    manager: payload.manager,
                    transition: 'return',
                    message: payload.message,
                },
            });
    });

    it('should call httpClient with certain params on authors', () => {
        const queryParams = {};
        queryString.mockReturnValueOnce(queryParams);
        const c = client(config, { defaultRequestOptions: {} });
        c.authors();
        expect(httpClient)
            .toHaveBeenCalledWith({
                method: 'GET',
                uri: 'testtest',
                qs: queryParams,
            });
    });

    it('should pass optional campaign_id parameter to query params on authors', () => {
        const campaign_id = 10;
        const queryParams = {};
        queryString.mockReturnValueOnce(queryParams);
        const c = client(config, { defaultRequestOptions: {} });
        c.authors({ campaign_id });
        expect(httpClient)
            .toHaveBeenCalledWith({
                method: 'GET',
                uri: 'testtest',
                qs: { ...queryParams, campaign_id },
            });
    });

    it('should call httpClient with certain params on getJobsByOrderId', () => {
        jobsQueryString.mockImplementation((...args) => args.join(''));
        const c = client(config, { defaultRequestOptions: {} });
        const payload = { id_order: 3 };
        c.getJobsByOrderId(payload);
        expect(httpClient)
            .toHaveBeenCalledWith({
                method: 'GET',
                uri: 'testtest',
                headers: this.headers,
                json: true,
                qs: { order_id: payload.id_order },
            });
    });

    it('should call httpClient with certain params on getCampaigns', () => {
        const queryParams = {};
        queryString.mockReturnValueOnce(queryParams);
        const c = client(config, { defaultRequestOptions: {} });
        c.getCampaigns();
        expect(httpClient)
            .toHaveBeenCalledWith({
                method: 'GET',
                uri: 'testtest',
                qs: queryParams,
            });
    });

    it('should pass optional manager parameter to query params on getCampaigns', () => {
        const manager = 'manager';
        const queryParams = {};
        queryString.mockReturnValueOnce(queryParams);
        const c = client(config, { defaultRequestOptions: {} });
        c.getCampaigns({ manager });
        expect(httpClient)
            .toHaveBeenCalledWith({
                method: 'GET',
                uri: 'testtest',
                qs: { ...queryParams, ldap_name: manager },
            });
    });

    it('should call httpClient with certain params on getWhiteLists', () => {
        const queryParams = {};
        queryString.mockReturnValueOnce(queryParams);
        const c = client(config, { defaultRequestOptions: {} });
        c.getWhiteLists();
        expect(httpClient)
            .toHaveBeenCalledWith({
                method: 'GET',
                uri: 'testtest',
                qs: queryParams,
            });
    });

    it('should pass optional campaign_id parameter to query params on getWhiteLists', () => {
        const campaign_id = 10;
        const queryParams = {};
        queryString.mockReturnValueOnce(queryParams);
        const c = client(config, { defaultRequestOptions: {} });
        c.getWhiteLists({ campaign_id });
        expect(httpClient)
            .toHaveBeenCalledWith({
                method: 'GET',
                uri: 'testtest',
                qs: { ...queryParams, campaign_id },
            });
    });

    it('should call httpClient with certain params on getOrderTextTypes', () => {
        queryString.mockImplementation((...args) => args.join(''));
        const c = client(config, { defaultRequestOptions: {} });
        c.getOrderTextTypes();
        expect(httpClient)
            .toHaveBeenCalledWith({
                method: 'GET',
                uri: 'testtest',
                qs: 'testtesttest',
            });
    });

    it('should add defaultRequestOptions to method call', () => {
        queryString.mockImplementation((...args) => args.join(''));
        const c = client(config, { defaultRequestOptions: { foo: 'bar' } });
        c.getOrderTextTypes();
        expect(httpClient)
            .toHaveBeenCalledWith({
                method: 'GET',
                uri: 'testtest',
                qs: 'testtesttest',
                foo: 'bar',
            });
    });
});

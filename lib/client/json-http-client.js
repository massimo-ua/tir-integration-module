const request = require('request-promise-native');
const { getQueryString, parseAddOrderResponce } = require('./helpers');

const JsonHTTPClient = (
    httpClient = request,
    queryString = getQueryString,
    addOrderParser = parseAddOrderResponce,
) => (config, {
    defaultRequestOptions = {
        headers: {
            Accept: 'application/json',
            'User-Agent': 'Booster-tir-client',
        },
        json: true,
    },
} = {}) => Object.freeze({

    async addOrder(payload) {
        const addOrderResponse = await httpClient({
            ...defaultRequestOptions,
            method: 'POST',
            uri: `${config.host}${config.path_add_order}`,
            qs: queryString(config.userName, config.path_add_order, config.userSecret),
            body: payload,
        });

        return addOrderParser(addOrderResponse);
    },

    approveJob({ jobId, manager }) {
        return httpClient({
            ...defaultRequestOptions,
            method: 'POST',
            uri: `${config.host}${config.path_update_job}`,
            qs: queryString(config.userName, config.path_update_job, config.userSecret),
            body: {
                job_id: jobId,
                manager,
                transition: 'confirm',
                message: '',
            },
        });
    },

    returnJob({ jobId, manager, message }) {
        return httpClient({
            ...defaultRequestOptions,
            method: 'POST',
            uri: `${config.host}${config.path_update_job}`,
            qs: queryString(config.userName, config.path_update_job, config.userSecret),
            body: {
                job_id: jobId,
                manager,
                transition: 'return',
                message,
            },
        });
    },

    authors({ campaign_id } = {}) {
        return httpClient({
            ...defaultRequestOptions,
            method: 'GET',
            uri: `${config.host}${config.path_get_authors}`,
            qs: {
                ...queryString(config.userName, config.path_get_authors, config.userSecret),
                ...(campaign_id && { campaign_id }),
            },
        });
    },

    getJobsByOrderId({ id_order: order_id }) {
        return httpClient({
            ...defaultRequestOptions,
            method: 'GET',
            uri: `${config.host}${config.path_get_jobs}`,
            headers: this.headers,
            json: true,
            qs: {
                ...queryString(
                    config.userName,
                    config.path_get_jobs,
                    config.userSecret,
                ),
                order_id,
            },
        });
    },

    getCampaigns({ manager: ldap_name } = {}) {
        return httpClient({
            ...defaultRequestOptions,
            method: 'GET',
            uri: `${config.host}${config.path_get_campaigns}`,
            qs: {
                ...queryString(config.userName, config.path_get_campaigns, config.userSecret),
                ...(ldap_name && { ldap_name }),
            },
        });
    },

    getWhiteLists({ campaign_id } = {}) {
        return httpClient({
            ...defaultRequestOptions,
            method: 'GET',
            uri: `${config.host}${config.path_get_whitelists}`,
            qs: {
                ...queryString(config.userName, config.path_get_whitelists, config.userSecret),
                ...(campaign_id && { campaign_id }),
            },
        });
    },

    getOrderTextTypes() {
        return httpClient({
            ...defaultRequestOptions,
            method: 'GET',
            uri: `${config.host}${config.path_get_texttypes}`,
            qs: queryString(config.userName, config.path_get_texttypes, config.userSecret),
        });
    },
});

module.exports = JsonHTTPClient;

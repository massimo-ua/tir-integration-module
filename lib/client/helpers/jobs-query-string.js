const { addDays, subDays, format } = require('date-fns');

module.exports = getQueryString => (userName, path, userSecret, queryPeriod) => ({
    ...getQueryString(userName, path, userSecret),
    from: format(subDays(Date.now(), queryPeriod), 'YYYY-MM-DD'),
    to: format(addDays(Date.now(), 1), 'YYYY-MM-DD'),
});

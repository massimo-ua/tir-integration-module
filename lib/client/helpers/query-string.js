const SHA256 = require('crypto-js/sha256');

module.exports = (user_name, path, userSecret, query_id = Date.now()) => ({
    user_name,
    query_id,
    signature: SHA256(`${user_name}${query_id}${path}${userSecret}`).toString(),
});

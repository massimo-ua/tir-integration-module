const SHA256 = require('crypto-js/sha256');
const { format } = require('date-fns');
const qs = require('./query-string');

describe('[QueryString]', () => {
    it('should be a function', () => {
        expect(typeof qs).toStrictEqual('function');
    });

    it('should return correct query object', () => {
        const user_name = 'q';
        const path = '/';
        const userSecret = 'q';
        const query_id = 1;
        const qo = qs(user_name, path, userSecret, query_id);
        expect(qo).toStrictEqual({
            user_name,
            query_id,
            signature: SHA256(`${user_name}${query_id}${path}${userSecret}`).toString(),
        });
    });

    it('should use Date.now to get default query_id', () => {
        const df = 'DD-MM-YYYY';
        const user_name = 'q';
        const path = '/';
        const userSecret = 'q';
        const qo = qs(user_name, path, userSecret);
        expect(format(qo.query_id, df)).toStrictEqual(format(Date.now(), df));
    });
});

const { addDays, subDays, format } = require('date-fns');
const JobsQueryString = require('./jobs-query-string');

const jqs = JobsQueryString(() => ({}));

describe('[JobsQueryString]', () => {
    it('should be a function', () => {
        expect(typeof jqs).toStrictEqual('function');
    });

    it('should return correct from and to params', () => {
        const queryPeriod = 1;
        const qo = jqs(null, null, null, queryPeriod);
        expect(qo).toStrictEqual({
            from: format(subDays(Date.now(), queryPeriod), 'YYYY-MM-DD'),
            to: format(addDays(Date.now(), 1), 'YYYY-MM-DD'),
        });
    });
});

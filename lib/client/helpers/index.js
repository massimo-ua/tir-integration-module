const getQueryString = require('./query-string');
const parseAddOrderResponce = require('./parse-add-order-responce');

module.exports = {
    getQueryString,
    parseAddOrderResponce,
};

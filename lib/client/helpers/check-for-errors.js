function checkForErrors({ errors }) {
    const applicationError = errors && new Error(errors.map(error => error.message));
    if (applicationError) {
        throw applicationError;
    }
}

module.exports = checkForErrors;

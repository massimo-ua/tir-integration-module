const checkForErrors = require('./check-for-errors');

function addOrderParser(addOrderResponse) {
    checkForErrors(addOrderResponse);
    return { data: { id_order: addOrderResponse.data.order } };
}

module.exports = addOrderParser;

const Client = require('./json-http-client');
const PreconfiguredClientFactory = require('./client-factory');

module.exports = PreconfiguredClientFactory(Client());

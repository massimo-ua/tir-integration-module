const ClientFactory = require('./client-factory');

const Client = jest.fn();

const configuration = {
    path_get_campaigns: '',
    path_get_whitelists: '',
    path_get_texttypes: '',
    path_get_jobs: '',
    path_get_authors: '',
    path_update_job: '',
    path_add_order: '',
    jobs_query_period: 1,
    userSecret: '',
    userName: '',
    host: '',
};

describe('[ClientFactory]', () => {
    let factory;

    beforeEach(() => {
        factory = ClientFactory(Client);
    });

    afterEach(() => {
        jest.restoreAllMocks();
    });

    it('should be a function', () => {
        expect(typeof factory).toStrictEqual('function');
    });

    it('should call Client and pass in configuration and options', () => {
        const opts = { test: 1 };
        factory(configuration, opts);
        expect(Client).toHaveBeenCalledWith(configuration, opts);
    });

    it('should pass to Client default options', () => {
        factory(configuration);
        expect(Client).toHaveBeenCalledWith(configuration, {});
    });
});

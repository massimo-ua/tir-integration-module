const ConfigurationsManager = require('./configurations-manager');

const Factory = jest.fn();
const defaultName = 'test';
const cache = new Map();

describe('[ConfigurationsManager]', () => {
    let cm;

    beforeEach(() => {
        cm = ConfigurationsManager(Factory, cache);
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should be an object', () => {
        expect(typeof cm).toStrictEqual('object');
    });

    it('should create ConfigurationsManager with default parameter values', () => {
        const cm2 = ConfigurationsManager(Factory);
        const configurations = cm2.listConfigurations();
        expect(configurations).toStrictEqual([]);
    });

    it('should call configurationsCache.set with name and factory output on setConfiguration', () => {
        const output = 'test';
        Factory.mockReturnValueOnce(output);
        const spy = jest.spyOn(cache, 'set');
        cm.setConfiguration({ name: 'name' });
        expect(spy).toHaveBeenCalledWith('name', output);
    });

    it('should call configurationsCache.set with default name if one was not passed on setConfiguration', () => {
        const spy = jest.spyOn(cache, 'set');
        cm.addDefaultConfigurationName(defaultName);
        cm.setConfiguration({});
        expect(spy).toHaveBeenCalledWith(defaultName, undefined);
    });

    it('should call configurationsCache.has with passed name on getConfiguration', () => {
        const spy = jest.spyOn(cache, 'has');
        cm.getConfiguration('name');
        expect(spy).toHaveBeenCalledWith('name');
    });

    it('should call configurationsCache.has with defaultName in one was not passed on getConfiguration', () => {
        const spy = jest.spyOn(cache, 'has').mockReturnValueOnce(true);
        jest.spyOn(cache, 'get').mockReturnValueOnce({});
        cm.addDefaultConfigurationName(defaultName);
        cm.getConfiguration();
        expect(spy).toHaveBeenCalledWith(defaultName);
    });

    it('should call configurationsCache.get with name if configurationsCache has configuration on getConfiguration', () => {
        jest.spyOn(cache, 'has').mockImplementation(() => true);
        const spy = jest.spyOn(cache, 'get').mockReturnValueOnce({});
        cm.setConfiguration(1, {});
        cm.getConfiguration(1);
        expect(spy).toHaveBeenCalledWith(1);
    });

    it('should throw error if configurationsCache has no configuration on getConfiguration', () => {
        jest.spyOn(cache, 'has').mockReturnValueOnce(false);
        expect(() => {
            cm.getConfiguration('qwerty');
        }).toThrow('TirConfigurationNotFound');
    });

    it('should call configurationsCache.keys on listConfigurations', () => {
        const spy = jest.spyOn(cache, 'keys');
        cm.listConfigurations();
        expect(spy).toHaveBeenCalled();
    });

    it('should call configurationsCache.clear on resetConfigurations', () => {
        const spy = jest.spyOn(cache, 'clear');
        cm.resetConfigurations();
        expect(spy).toHaveBeenCalled();
    });
});

const ClientFactory = require('../client');
const ConfigurationsManager = require('./configurations-manager');

module.exports = ConfigurationsManager(ClientFactory);

let DEFAULT_CONFIGURATION_NAME;
const ConfigurationsManager = (Factory, configurationsCache = new Map()) => ({
    addDefaultConfigurationName(name) {
        if (configurationsCache.has(DEFAULT_CONFIGURATION_NAME)) {
            configurationsCache.delete(DEFAULT_CONFIGURATION_NAME);
        }
        DEFAULT_CONFIGURATION_NAME = name;
    },
    setConfiguration({ name = DEFAULT_CONFIGURATION_NAME, ...options }) {
        configurationsCache.set(name, Factory(options));
    },
    getConfiguration(name = DEFAULT_CONFIGURATION_NAME) {
        const configuration = configurationsCache.has(name)
            ? configurationsCache.get(name)
            : configurationsCache.get(DEFAULT_CONFIGURATION_NAME);
        if (configuration) {
            return configuration;
        }
        throw new Error('TirConfigurationNotFound');
    },
    listConfigurations() {
        return Array.from(configurationsCache.keys());
    },
    resetConfigurations() {
        configurationsCache.clear();
    },
});

module.exports = ConfigurationsManager;

module.exports = {
    get: (target, name) => (
        (name in target)
            ? target[name]
            : target.makeRequest.bind(target, name)
    ),
};

const Api = configurationsManager => ({
    setConfigurations(configurations) {
        configurations.forEach(
            configuration => configurationsManager.setConfiguration(configuration),
        );
    },
    addDefaultConfigurationName(name) {
        configurationsManager.addDefaultConfigurationName(name);
    },
    updateConfiguration(configuration) {
        configurationsManager.setConfiguration(configuration);
    },
    listConfigurations() {
        return configurationsManager.listConfigurations();
    },
    resetConfigurations() {
        configurationsManager.resetConfigurations();
    },
    async makeRequest(method, configurationName, payload) {
        const client = configurationsManager.getConfiguration(configurationName);
        const { data, errors = [] } = await client[method](payload);
        if (data !== undefined && errors.length === 0) {
            return data;
        }
        const [error = 'TIR application error'] = errors;
        const appError = new Error(error);
        appError.details = errors;
        throw appError;
    },
});

module.exports = Api;

const Api = require('./module-public-api');

const MokedConfigurationsManager = {
    setConfiguration() {},
    listConfigurations() {},
    resetConfigurations() {},
    getConfiguration() {},
    addDefaultConfigurationName() {},
};

describe('[Api]', () => {
    let api;
    beforeEach(() => {
        api = Api(MokedConfigurationsManager);
    });

    afterEach(() => {
        jest.restoreAllMocks();
    });

    it('should be a defined', () => {
        expect(api).toBeDefined();
    });

    it('should call configurationsManager.setConfiguration for every configuration item on setConfigurations', () => {
        const spy = jest.spyOn(MokedConfigurationsManager, 'setConfiguration');
        api.setConfigurations([{}, {}]);
        expect(spy).toHaveBeenCalledTimes(2);
    });

    it('should call configurationsManager.setConfiguration with configuration passed in on setConfigurations', () => {
        const spy = jest.spyOn(MokedConfigurationsManager, 'setConfiguration');
        const configuration = { test: 1 };
        api.setConfigurations([configuration]);
        expect(spy).toHaveBeenCalledWith(configuration);
    });

    it('should call configurationsManager.setConfiguration with configuration passed in on updateConfiguration', () => {
        const spy = jest.spyOn(MokedConfigurationsManager, 'setConfiguration');
        const configuration = { test: 1 };
        api.updateConfiguration(configuration);
        expect(spy).toHaveBeenCalledWith(configuration);
    });

    it('should call configurationsManager.listConfigurations on listConfigurations', () => {
        const spy = jest.spyOn(MokedConfigurationsManager, 'listConfigurations');
        api.listConfigurations();
        expect(spy).toHaveBeenCalled();
    });

    it('should call configurationsManager.resetConfigurations on resetConfigurations', () => {
        const spy = jest.spyOn(MokedConfigurationsManager, 'resetConfigurations');
        api.resetConfigurations();
        expect(spy).toHaveBeenCalled();
    });

    it('should call configurationsManager.getConfiguration on makeRequest', () => {
        const confName = 'test';
        const spy = jest.spyOn(MokedConfigurationsManager, 'getConfiguration').mockImplementation(() => ({
            test() {
                return { data: 1 };
            },
        }));
        api.makeRequest('test', confName);
        expect(spy).toHaveBeenCalledWith(confName);
    });

    it('should call client[method] and pass in payload on makeRequest', () => {
        const spy = jest.fn().mockImplementation(() => ({ data: 1 }));
        jest.spyOn(MokedConfigurationsManager, 'getConfiguration').mockImplementation(() => ({
            test: spy,
        }));
        api.makeRequest('test', null, 1);
        expect(spy).toHaveBeenCalledWith(1);
    });

    it('should throw error if response does not contains data property on makeRequest', async () => {
        expect.assertions(1);
        jest.spyOn(MokedConfigurationsManager, 'getConfiguration').mockImplementation(() => ({
            test: () => ({}),
        }));
        await api.makeRequest('test', null, 1).catch((e) => {
            expect(e.message).toStrictEqual('TIR application error');
        });
    });

    it('should set call configurationsManager.addDefaultConfigurationName on  addDefaultConfigurationName', () => {
        const configurationName = 'test';
        const spy = jest.spyOn(MokedConfigurationsManager, 'addDefaultConfigurationName');
        api.addDefaultConfigurationName(configurationName);
        expect(spy).toHaveBeenCalledWith(configurationName);
    });
});

const configurationsManager = require('../configurations-manager');
const PublicApi = require('./module-public-api');
const apiRequestsHandler = require('./api-query-handler');

const api = PublicApi(configurationsManager);
module.exports = new Proxy(api, apiRequestsHandler);

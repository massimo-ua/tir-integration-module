const handler = require('./api-query-handler');

describe('[ApiQueryHandler]', () => {
    it('should be a function', () => {
        expect(typeof handler).toStrictEqual('object');
    });

    it('should return object property if object has one', () => {
        const target = { test: 1 };
        expect(handler.get(target, 'test')).toStrictEqual(1);
    });

    it('should call target makeRequest and bind passed in name as first parameter if target has no property', () => {
        const prop = 'test';
        const target = { makeRequest(name) { return name; } };
        const res = handler.get(target, prop);
        expect(res()).toStrictEqual(prop);
    });
});
